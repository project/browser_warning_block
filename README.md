# Browser Warning Block

[TOC]

## Introduction
This module suplies a block that displays a warning if a certain browser is 
detected.

## Description
You can select the browser for which a block is displayed to inform the user 
that their browser isn't supported. 

## Requirements
drupal/browscap

## Installation
Install as you would normally install a Drupal module.

## Configuration
Place block and choose the broswer for which this block is displayed.

## Permissions
Inapplicable.

## Tests
Inapplicable.

## Credits
Inapplicable.

## Authors
This module was created by:  
* [Immoreel](drupal@immoreel.nl)  
