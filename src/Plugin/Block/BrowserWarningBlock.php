<?php

namespace Drupal\browser_warning_block\Plugin\Block;

use Drupal\browscap\BrowscapService;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'BrowserWarningBlock' block.
 *
 * @Block(
 *  id = "browser_warning_block",
 *  admin_label = @Translation("Browser warning block"),
 * )
 */
class BrowserWarningBlock extends BlockBase implements ContainerFactoryPluginInterface {

  protected $browscap;

  /**
   * Constructs a new UserNameBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\browscap\BrowscapService $browscapService
   *   The browsecap service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BrowscapService $browscapService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->browscap = $browscapService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('browscap'));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'browser_warning_block_message' => 'The browser you are using is unsupported, please upgrade to a more modern browser',
      'browser_warning_block_browser' => ['IE' => $this->t('Internet Explorer')],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['browser_warning_block_message'] = [
      '#type'          => 'text_format',
      '#title'         => $this->t('Message'),
      '#description'   => $this->t('Enter the message you want to display if an unsupported browser is used'),
      '#default_value' => $this->configuration['browser_warning_block_message'],
      '#weight'        => '0',
    ];
    $form['browser_warning_block_browser'] = [
      '#type'          => 'checkboxes',
      '#options'       => [
        'IE'      => $this->t('Internet Explorer'),
        'Firefox' => $this->t('Firefox'),
      ],
      '#title'         => $this->t('Show the block for the following browsers'),
      '#description'   => $this->t('Check the browser for which you want to display the warning block.'),
      '#default_value' => $this->configuration['browser_warning_block_browser'],
      '#weight'        => '1',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['browser_warning_block_message'] = $form_state->getValue('browser_warning_block_message')['value'];
    $this->configuration['browser_warning_block_browser'] = $form_state->getValue('browser_warning_block_browser');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $browser = $this->browscap->getBrowser();

    if ($browser['browser'] === 'IE') {
      return [
        '#theme' => 'browser_warning_block',
        '#content' => $this->configuration['browser_warning_block_message'],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
